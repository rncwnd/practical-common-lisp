;; Macro control constructs

;; IF
(defun yup-nope (a b)
  "If a > b print yup. else nope"
  (if (> a b) "Yup" "Nope"))

(defun yup-nil (a b)
  "If a > b print yup. else nil"
  (if (> a b) "Yup"))

;; Cond
(defun cond-test (a)
  (cond ((= a 5) (format t "A = 5"))
        ((= a 10) (format t "A = 10"))
        ((= a 15) (format t "A = 15"))))

;; AND OR NOT
(not nil)
(not (= 1 1))
(and (= 1 1) (= 2 2))
(and (= 1 2) (= 2 2))
(or (= 1 2) (= 2 2))

;; DO Macros
;; DOLIST

(defun dolist-test ()
  (dolist (x '(1 2 3))
    (print x)))

(defun dolist-break ()
  (dolist (x '(1 2 3))
    (print x)
    (if (evenp x) (return))))

(defun dotimes-test (x)
  (dotimes (i x)
    (print i)))

(defun nested-dotimes ()
  (dotimes (x 20)
    (dotimes (y 20)
      (format t "~3d " (* (1+ x) (1+ y))))
    (format t "~%")))

;; DO
;; (do (variabledefs)
;;   (end-test-form result-form)
;; statement)

(defun fib (max)
  (do ((n 0 (1+ n))
       (curr 0 next)
       (next 1 (+ curr next)))
      ((= max n ) curr)))

;; LOOP
(loop for i from 1 to 10 collecting i)
(loop for i from 1 to 10 summing (expt i 2))
(loop for x across "the quick brown fox jumps over the lazy dog"
      counting (find x "aeiou"))
(loop for i below 10
      and a = 0 then b
      and b = 1 then (+ b a)
      finally (return a))
