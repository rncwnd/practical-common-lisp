
;; Defines a simple function to check if 1 == 1.
;; (if (condition) (then-form) (else-form))
;; If is a macro
(defun one-is-one ()
 (if (= 1 1) (format t "yes") (format t "no")))
