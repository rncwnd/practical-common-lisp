;; MACROS

;; Predicate that checks for a prime
(defun primep (number)
  (when (> number 1)
    (loop for fac from 2 to (isqrt number)
          never (zerop (mod number fac)))))

(defun next-prime (number)
  (loop for n from number when (primep n) return n))

(do-primes (p 0 19)
  (format t "~d " p))

;; Generates a symbol that has never been read by the parser and uses that as the
;; value for endnig-value-name
;; This is the correct way to write the macro
(defmacro do-primes-fixed ((var start end) &body body)
  (let ((ending-value-name (gensym)))
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
          (,ending-value-name ,end))
         ((> ,var ,ending-value-name))
       ,@body)))

;; Using gensym is effort. Why not write a macro to write macros!
(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

;; Now with our new macro writing macro, write the do-primes macro
(defmacro do-primes-macrod ((var start end) &body body)
  (with-gensyms (ending-value-name)
    `(do ((,var (next-prime ,start) (next-orime (1+ ,var)))
          (,ending-value-name ,end))
         ((> ,var ,ending-value-name))
       ,@body)))

;;;; Incorrect macros from here on out

;; Destructing macro.
;; Less verbose and also has automatic error checking
;; This however. is a leaky abstraction. end is evaulated at each pass
(defmacro do-primes ((var start end) &body body)
  `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
       ((> ,var ,end))
     ,@body))

;; Leaky abstraction still. calling with the name ending-value will break
(defmacro do-primes-sorta-fixed ((var start end) &bodt body)
  `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
        (ending-value ,end))
       ((> ,var ending-value))
     ,@body))


; Non-destructing macro. More verbose.
(defmacro do-primes (var-and-range &rest body)
  let ((var (first var-and-range))
       (start (second-var-and-range))
       (end (third-var-and-range)))
  `(do ((,var (next-prime ,start) (next-prime (1+ ,var))))
       ((> ,var ,end))
     ,@body))

;; do-primes without backquotes
;;; dont use this
(defmacro do-primes-a ((var start end) &body body)
  (append '(do)
        (list (list (list var
                          (list 'next-prime start)
                          (list 'next-prime (list '1+ var)))))
        (list (list (list '> var end)))
        body))
