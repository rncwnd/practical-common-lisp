;;;; Chapter 9
;;;; Unit Test Framework

(defvar *test-name* nil)

;;; Bad unit test. It just werks :tm: but the reporting sucks
(defun test-+ ()
  (and
   (= (+ 1 2) 3)
   (= (+ 1 2 3) 6)
   (= (+ -1 -3) -4)))

;;; Still bad but better. tests each case and reports
;;; Repetative af tho
(defun test-+-fmt ()
  (format t "~:[FAIL~;pass~] ... ~a~%"
          (= (+ 1 2) 3) '(= (+ 1 2) 3))
  (format t "~:[FAIL~;pass~] ... ~a~%"
          (= (+ 1 2 3) 6) '(= (+ 1 2 3) 6))
  (format t "~:[FAIL~;pass~] ... ~a~%"
          (= (+ -1 -3) -4) '(= (+ -1 -3) -4)))

;;; Better
(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a~%" result form))

(defun test-+-functional ()
  (report-result (= (+ 1 2) 3) '(= (+ 1 2) 3))
  (report-result (= (+ 1 2 3) 6) '(= (+ 1 2 3) 6))
  (report-result (= (+ -1 -3) -4) '(= (+ -1 -3) -4)))

;;; But there's still duplication. You want to treat code as data
;;; Use a macro!
(defmacro check (form)
  `(report-result ,form ',form))

(defun test-+-macro ()
  (check (= (+ 1 2) 3))
  (check (= (+ 1 2 3) 6))
  (check (= (+ -1 -3) -4)))

;;; Good, but now you're still calling check 3 times.
;;; Make check take an arbutrary number of forms!
(defmacro check-arb (&body forms)
  `(progn
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defun test-+-arb ()
  (check-arb
   (= (+ 1 2) 3)
   (= (+ 1 2 3) 6)
   (= (+ -1 -3) -4)))

(defun report-result-return (result form)
  (format t "~:[FAIL~;pass~] ... ~a~%" result form)
  result)

;;; Copied from ch8
(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

;;; EVEN MORE MACRO ABUSE
(defmacro combine-results (&body forms)
  (with-gensyms(result)
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result)))

(defmacro check-combine (&body forms)
  `(combine-results
     ,@(loop for f in forms collect `(report-result-return ,f ',f))))

(defun test-+ ()
  (check-combine
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)
    (= (+ -1 -3) -4)))

;;; Add a new test
(defun test-* ()
  (check-combine
    (= (* 2 2) 4)
    (= (* 3 5) 15)))

;;; Oh hey. You can reuse macros for multiple purposes.
(defun test-arithmatic ()
  (combine-results
    (test-+-arb-macroed)
    (test-*)))

;;; Make a macro that defines an entire test!
(defmacro deftest (name parameters &body body)
  `(defun ,name ,parameters
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

;;; Now we have a macro to define an entire test. rewrite test-+ with that macro
(deftest test-+ ()
  (check-combine
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)
    (= (+ -1 -3) -4)))

(deftest test-arithmatic ()
  (check-combine
    (test-+)
    (test-*)))

(deftest test-maths ()
  (test-arithmatic))
