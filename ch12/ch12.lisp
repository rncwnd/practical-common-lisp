
;;; Recycling functions are a gun pointed towards your foot.
;;; There are idomatic uses that simply state where it's good to use them or not

;; Build a list of the first n numbers. Because the list is only created inside
;; The function there is no danger of code outside of it using the cons cells that it itself uses

(defun upto (max)
  (let ((result nil))
    (dotimes (i max)
      (push i result))
    (nreverse result)))

;;; Another common recycling idom is immediatley reassigning the value returned by recycling.
;;; This must still be used with care though. Any lists sharing cons cells with the deleted list
;;; Will have their structure changed.
(setf foo (delete nil foo))

;;; Big gotcha. Sorts and merges are recycling when applied to lists
;;; If you need to sort a list non-destructivley use copy-list first

(defparameter *list* (list 4 3 2 1))
(sort *list* #'<) ; ==> (1 2 3 4) looks good!
(*list*)          ; ==> (4). Oh...

;;; list's support a mapping system like the other containers. mapcar is the most like map.
;;; Takes a function to apply and the lists to apply it to.
(mapcar #'(lambda (x) (* 2 x)) (list 1 2 3)) ;; ==> (2 4 6)
(mapcar #'+ (list 1 2 3) (list 10 20 30))    ;; ==> (11 22 33)

;;; Maplist is pretty much the same; however it passes the cons cells to the function rather
;;; Than the elements of the list. This means that it can access each element,
;;; as well as the rest of the list via the CDR of the cell.

;;; mancan and mapcon work like mapcar and maplist except instead of building a new lists
;;; They splice together the results as if by nconc

;;; mapc and mapl are control constructs. they return their first list argument.
;;; Useful for when the side effects of a mapped function do something interesting.
