(defun let-test (x)
  (format t "Parameter: ~a~%" x)
  (let ((x 2))
    (format t "Outer let: ~a~%" x)
    (let ((x 3))
      (format t "Inner let: ~a~%" x))
    (format t "Outer let: ~a~%" x))
  (format t "Parameter: ~a~%" x))

;; let only allows a variable to be used in the body of the let
;; let* on the other hand, lets you use the initial value earlier in the var list.
(defun valid ()
    (let* ((x 10)
       (y (+ x 10)))
  (list x y)))

;;(defun invalid ()
  ;;(let ((x 10)
        ;;(y (+ x 10))) ;;x is undefined here as variables are bound based on the smallest scope
    ;;(list x y)))

(defun valid-with-multiple-let ()
    (let ((x 10))
      (let ((y (+ x 10)))
        (list x y))))

(defun closure-list () "Return a list of 3 closures"
  (let ((count 0))
    (list
     #'(lambda () (incf count)) ;; Returns a closure that increments
     #'(lambda () (decf count)) ;; Returns a closure that decrements
     #'(lambda () count))))     ;; Returns the current value of count

(defvar *count* 0                 ;; Creates a value called count. Only assings value to 0 if undefined
  "Count of widgets made so far")

(defparameter *gap-tolerance* 0.001 ;; Creates a global. Always assigns the initial value
  "Tolerance for widget gaps")

(defun incriment-widget-count ()
  "Increment the count of the global widget count"
  (incf *count*))

(defvar *x* 10)
(defun dynamic-shadow()
  (format t "X: ~d~%" *x*))

(defun dynamic-test()
  (dynamic-shadow)
  (let ((*x* 20)) (dynamic-shadow))
  (dynamic-shadow))

(defun setx-10 (z)
  (setf z 10))

(defun rotate-test (a b)
  (format t "Before rotate ~C" #\linefeed)
  (format t "a: ~d~%b: ~d~%" a b)
  (rotatef a b)
  (format t "After rotate ~C" #\linefeed)
  (format t "a: ~d~%b: ~d~%" a b))
