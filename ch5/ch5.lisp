(defun verbose-sum (x y)
  "Sum any two numbers after priting a message"
  (format t "Summing ~d and ~d.~%" x y)
  (+ x y))

(defun optionaltest (a b &optional c d)
  (list a b c d))

(defun optionalsetret (a &optional (b 42))
  (list a b))

(defun suppliedtest (a b &optional (c 3 c-supplied-p) d)
  (list a b c c-supplied-p d))

;; Initialize based on if the optional parameter is passed in.
;; If it is, then set it to user input. Else define it was the same as width
(defun make-rectangle (width &optional (height width))
  (list width height))

(defun rest-test (a b &rest values)
  (write(list a b values)))

(defun key-test (&key a b c)
  (list a b c))

(defun key-default (&key (a 0) (b 0 b-supplied-p) (c (+ a b)))
  (list a b c b-supplied-p))

(defun keyword-names (&key ((:apple a)) ((:box b)) ((:charlie c) 0 c-supplied-p))
  (list a b c c-supplied-p))

(defun returns (n)
  (dotimes (i 10)
    (dotimes (j 10)
      (when (> (* i j) n)
        (return-from returns (list i j))))))
